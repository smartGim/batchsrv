package cn.gitchat.share.model;

import lombok.Data;

@Data
public class PayRecordExt extends PayRecord {
    private String ext;
}
